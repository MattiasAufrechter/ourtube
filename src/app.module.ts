import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { MulterModule } from '@nestjs/platform-express';
import { VideoModule } from './video/video.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/ourtube'),
    UserModule,
    AuthModule,
    MulterModule.register({
      dest: './uploads',
    }),
    VideoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
