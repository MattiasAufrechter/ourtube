import { Controller, Req } from '@nestjs/common';
import { Get } from '@nestjs/common';
import { Post } from '@nestjs/common';
import { UseInterceptors } from '@nestjs/common';
import { UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Param } from '@nestjs/common';
import { Res } from '@nestjs/common';
import { saveImageToStorage } from './helpers/video-storage';
import { VideoService } from './video.service';

@Controller('video')
export class VideoController {

  constructor(private videoService: VideoService) {}
  @Post('upload')
  @UseInterceptors(FileInterceptor('file', saveImageToStorage))
  uploadFile(@UploadedFile() file) {
    console.log(file);
    this.videoService.createVideo(file.filename)
    return "video uploaded"
  }

  @Get(':filepath')
  seeuploadedFile(@Param('filepath') file, @Res() res, @Req() req) {
    // this.videoService.addOne()
    return res.sendFile(file, { root: './uploads' });
  }
}
