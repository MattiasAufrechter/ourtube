import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VideoController } from './video.controller';
import { VideoSchema } from './video.Schema';
import { VideoService } from './video.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Video', schema: VideoSchema }]),
    ],
    controllers : [VideoController],
    providers: [VideoService]
})
export class VideoModule {}
