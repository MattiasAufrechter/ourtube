import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { VideoDocument } from './video.Schema';

import { Model } from 'mongoose';

@Injectable()
export class VideoService {
    constructor(@InjectModel('Video') private readonly videoModel: Model<VideoDocument>,){}

    async createVideo(title: string): Promise<VideoDocument> {
        const newVideo = new this.videoModel({ title });
        return newVideo.save();
    }

    async addOne(){
        const add = await this.videoModel.findOne({view: Number}, { $set: {view: +1 }}).exec()
        return add
    }
}
