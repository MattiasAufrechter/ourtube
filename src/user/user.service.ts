import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './user.Schema';
import { UserDetails } from './user-details.interface';
import { Promise } from 'mongoose';

@Injectable()
export class UserService {
    constructor(@InjectModel('User') private readonly userModel: 
    Model <UserDocument>,
    ){}
    _getUserDetails(user: UserDocument): UserDetails {
        return{
            id: user._id,
            name: user.name,
            email: user.email,
        }
    }

    async findByEmail(email: string): Promise<UserDocument | null>{
        return this.userModel.findOne({email}).exec()
    }

    async findById(id: string): Promise<UserDetails | null>{
        const user = await this.userModel.findById(id).exec();
        if (!user) return null;
        return this._getUserDetails(user);
    }

    async create(
        name: string, 
        email: string, 
        hashedPassword: string
        ): Promise<UserDocument> {
        const newUser = new this.userModel({
            name, 
            email, 
            password: hashedPassword
        });
    return newUser.save()
    }
    async delete(id: string): Promise <UserDocument>{
        const user = await this.userModel.findByIdAndDelete(id).exec();
        if(!user){
            throw new NotFoundException(`User ${id} is not found`);
        }
        return user;
    }

    async find(id:string): Promise<UserDocument>{
        return this.userModel.findById(id).exec();
    }

    async updateUser(id: string, newName: string): Promise <UserDocument> {
        const existingUser = await this.find(id);
        existingUser.name = newName ?? existingUser.name

        return existingUser.save();
    }
}
