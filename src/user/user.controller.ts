import { Controller, Delete, Get, Patch, Put } from '@nestjs/common';
import { Body, Param } from '@nestjs/common/decorators/http/route-params.decorator';
import { UserDetails } from './user-details.interface';
import { UserDocument } from './user.Schema';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private UserService: UserService) {}
        @Get(':id')
        getUser(@Param('id') id: string): Promise<UserDetails | null> {
            return this.UserService.findById(id);
        }
        @Patch(':id')
        update(@Param('id') id: string, @Body('name') name: string): Promise<UserDocument | null>{
            return this.UserService.updateUser(id, name);
        }
        @Delete(':id')
            delete(@Param('id') id: string) {
                return this.UserService.delete(id);
            }
}
